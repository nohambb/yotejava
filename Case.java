import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.String.valueOf;

public class Case extends JButton implements ActionListener {

    private int typeCase;  // si typeCase == 0, case du plateau, si typeCase ==1 case du stock

    private ImageIcon imagePion;
    private Pion pion;
    private Color couleurFond;


    private boolean occupe;
    private int abscisse; 
    private int ordonnee;

    public Case(Color couleur, int abs, int ord, int typeCase) {
    	this.couleurFond = couleur;
    	this.setBackground(couleur);
        pion = null;
        imagePion = null;
        occupe = false;
        abscisse = abs;
        ordonnee = ord;
        this.typeCase = typeCase;
        if (typeCase == 0)
            this.setPreferredSize(new Dimension(100, 100));
        else
            this.setPreferredSize(new Dimension(100, 100));

        addActionListener(this);
    }

    public Pion getPion() {
	return pion;
    }

    public void setPion(Pion p) {
    	if (p!= null) {
    	pion = p;
    	if (pion.getCouleur()== CouleurPion.blanc)
    		imagePion = new ImageIcon ("Icones/zebre.png");
    	else
    		imagePion = new ImageIcon ("Icones/guepard.png");        
        this.setIcon(imagePion);
        occupe=true;
        }else {
    		pion = null;
            imagePion = null;
            occupe = false;
            this.setIcon(imagePion);      	
    	}	
    }
    public int getAbscisse() {
    	return abscisse;
    }

    public int getTypeCase() {
    	return typeCase;
    }

    public int getOrdonnee() {
    	return ordonnee;
    }

    public boolean isOccupe() {
    	return occupe;
    }


    public void actionPerformed(ActionEvent e) {
        if (Yote.etat == 0) {
            // reactiver le bouton annuler
            Fenetre.boutonAnnuler.setEnabled(false);

            // on selectionne une case de depart
            Yote.caseDep = ((Case) e.getSource());
           
            if (Yote.caseDep.isOccupe()) {
                if (Yote.caseDep.getPion().getCouleur() == Yote.joueur) {
                    // si la case selectionne pour le depart du deplacement est valide : elle est occupee par un Pion de la couleur du joueur dont c'est le tour, on peut selectionner la case d'arrivee
                    Yote.caseDep.setBorder(Yote.redline);
                    Yote.etat = 1; // on passe à l'etat 1
                }
            }
        } else {
            if (Yote.etat == 1) {
		// A completer
		
		// identification de la case possible d'arrivée
                Yote.caseArr = ((Case) e.getSource());
                
                if (Yote.unPlateau.isIn(Yote.caseArr)) {
		    //la case d arrivee sélectionnée est dans le plateau
		    // on teste si la case selectionnee est differente de la case de depart
                    // si le coup est possible on le joue
                	if (Yote.caseDep !=Yote.caseArr){
                		if (Yote.caseDep.getTypeCase()==1) {
                			if (Yote.caseArr.isOccupe()==false){
                				Yote.unPlateau.jouerCoup(Yote.caseDep,Yote.caseArr);
                				Yote.etat = 0;
                            	Yote.caseDep.setBorder(Yote.empty);
                            	if (Yote.joueur == CouleurPion.blanc)
                            		Yote.joueur = CouleurPion.noir;
                            	else
                            		Yote.joueur = CouleurPion.blanc;
                			}
                			
                		}else {
                			if(Yote.unPlateau.coupValide(Yote.caseDep, Yote.caseArr)!=0) {
                    			Yote.unPlateau.jouerCoup(Yote.caseDep,Yote.caseArr);
                    			Yote.etat = 0;
                            	Yote.caseDep.setBorder(Yote.empty);
                            	if (Yote.joueur == CouleurPion.blanc)
                            		Yote.joueur = CouleurPion.noir;
                            	else
                            		Yote.joueur = CouleurPion.blanc;
                    		}	
                		}
                		
                	}
                	if (Yote.unPlateau.casePrise(Yote.caseDep, Yote.caseArr)!=null){
                    	Yote.unPlateau.jouerCoupPrise(Yote.caseDep, Yote.caseArr);
                    	Yote.etat=2;
                	}
                	
                	
                	
		    // si on prend un pion on va dans l'etat 2
                }
		else {
                    //la case d arrivee sélectionnée n est pas dans le plateau

                    
                }
            }
	    else {
                if (Yote.etat == 2) {
		    // A completer
                	Yote.casePrise = ((Case)e.getSource());
                	if (Yote.caseDep.getTypeCase()==0 || Yote.caseDep.getTypeCase()==1){
                		if (Yote.casePrise.isOccupe()==true){
                			if (Yote.joueur == CouleurPion.blanc)
                				Yote.joueur = CouleurPion.noir;
                			else
                        		Yote.joueur = CouleurPion.blanc;
                			if (Yote.caseArr.isOccupe()==false) {
                				if (Yote.casePrise.getPion().getCouleur() != Yote.joueur){
                    				Yote.casePrise.setPion(null);
                    			}
                			}
                		}
                	}
		     boolean finPArtie = false;
                    
                    CouleurPion couleurSuiv = CouleurPion.blanc;
                    if (Yote.joueur == CouleurPion.blanc)
                        couleurSuiv = CouleurPion.noir;
		  
		    // Choix du deuxième pion à prendre
		  
		  
		    // si sur la case cliquée la pièce est de la bonne couleur, on décremente le nombre de pion du joueur et on test si la partie est finie 
                    
                    if (finPArtie) {
                            //ouvrir une boite de dialogue pour signifier que le partie est finie : 2 choix fermer le jeu ou recommencer
                            JOptionPane d = new JOptionPane(); // les textes figurant // sur les boutons
                            String lesTextes[] = {"Recommencer", "Fermer le jeu"}; // indice du bouton qui a été // cliqué ou CLOSED_OPTION
                            int retour = d.showOptionDialog(this, "Partie terminée. Le joueur " + Yote.joueur + " a perdu !", "Fin de jeu", 1, 1, new ImageIcon(), lesTextes, lesTextes[0]);
                            if (retour == 0) {
                                Yote.unPlateau.reinitialiser();
                            } else {
                                Yote.fenetrePrincipale.dispose();
                            }
                        }

		    // mise à jour des scores 
		       Yote.scoreBlanc.setText("Score Joueur Blanc \n "+ valueOf(Yote.nbPionBlanc));
                        Yote.scoreNoir.setText("Score Joueur Noir \n" + valueOf(Yote.nbPionNoir));

                        Yote.fenetrePrincipale.repaint();
                        Yote.fenetrePrincipale.validate();

                        
                        Fenetre.boutonAnnuler.setEnabled(true);

			Yote.joueur = couleurSuiv;
			// on retourne dans l'état 0
			Yote.etat = 0;
                    }
	    }
	}
    }


    public String toString(){
	// A completer
	return "";
    }
}