import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.String.valueOf;

public class Plateau extends JPanel {
    private Case[][] monPlateau;

    
    public Plateau() {
	    this.setLayout(new GridLayout (5,6));
	    monPlateau = new Case[5][6];
	    for (int i=0; i<5; i++) {
	   		for (int j=0; j<6; j++) {
	   			monPlateau[i][j]= new Case(Color.white, i, j, 0);
	    		this.add(monPlateau[i][j]);
	    		monPlateau[i][j].setBorder(Yote.empty);
	    	}
	    }
    }

    public boolean isIn(Case c){
		if (c.getTypeCase()==0) {
			return true;
		}else {
			return false;
		}
    }
 
    public int coupValide(Case dep, Case arr) {//Teste si la Pion qui est en dep peut de deplacer en arr : renvoie 0 si deplacement interdit, 1 si deplacement valide, 2 si prise valide
    	if (arr.isOccupe()==true) {
    		return 0;
    	}else if (verifieDeplacementVertPrise(dep,arr)==true) {
    		return 2;
    	}else if(verifieDeplacementHorPrise(dep,arr)==true){
    		return 2;
    	}else if(verifieDeplacementVert(dep,arr)==true) {
    		return 1;
    	}else if(verifieDeplacementHor(dep,arr)==true) {
    		return 1;
    	}else {
    		return 0;
    	}
    }

    public boolean verifieDeplacementVertPrise(Case dep, Case arr){
    	if (dep.getOrdonnee()==arr.getOrdonnee()+2 && dep.getAbscisse()==arr.getAbscisse()){
    		int i=dep.getAbscisse();
    		int j=dep.getOrdonnee()-1;
    		Case m = monPlateau[i][j];
    		if (m.isOccupe()==true){
    			if (CouleurPion.blanc != CouleurPion.noir) {
    				return true;
    			}
    		}
    	}
    	
    	if (dep.getOrdonnee()==arr.getOrdonnee()-2 && dep.getAbscisse()==arr.getAbscisse()) {
    		int i=dep.getAbscisse();
    		int j=dep.getOrdonnee()+1;
    		Case m = monPlateau[i][j];
    		if (m.isOccupe() ==true ) {
    			if (CouleurPion.blanc != CouleurPion.noir) {
    					return true;
    				}
    			}
    		}
    	
    	return false;
        }
    

    public boolean verifieDeplacementHorPrise(Case dep, Case arr){
    
	if (dep.getAbscisse()==arr.getAbscisse()+2 && dep.getOrdonnee()==arr.getOrdonnee()) {
		int i=dep.getAbscisse()-1;
		int j=dep.getOrdonnee();
		Case m = monPlateau[i][j];
		if (m.isOccupe() ==true ) {
			if (CouleurPion.blanc != CouleurPion.noir) {
				return true;
			}
		}
	}
	
	if (dep.getAbscisse()==arr.getAbscisse()-2 && dep.getOrdonnee()==arr.getOrdonnee()) {
		int i=dep.getAbscisse()+1;
		int j=dep.getOrdonnee();
		Case m = monPlateau[i][j];
		if (m.isOccupe()==true) {
			if (CouleurPion.blanc != CouleurPion.noir) {
					return true;
				}
			}
		}
	
	return false;
    }
    
    boolean verifieDeplacementVert(Case dep, Case arr){
    	if (dep.getOrdonnee()==(arr.getOrdonnee()+1) || dep.getOrdonnee()==(arr.getOrdonnee()-1)) {
    		if (dep.getAbscisse()==arr.getAbscisse()) {
    			return true;
    		}
		}
		return false;
    } 

    public boolean verifieDeplacementHor(Case dep, Case arr){
    	if (dep.getAbscisse()==(arr.getAbscisse()+1) || dep.getAbscisse()==(arr.getAbscisse()-1)) {
    		if (dep.getOrdonnee()==arr.getOrdonnee()) {
    			return true;
    		}
    	}
    	return false; 
    }

    public Case casePrise(Case dep, Case arr){
    	if (dep.getAbscisse()==arr.getAbscisse()+2 && dep.getOrdonnee()==arr.getOrdonnee()) {
    		return monPlateau [dep.getAbscisse()-1][dep.getOrdonnee()];
    	}else if (dep.getAbscisse()==arr.getAbscisse()-2 && dep.getOrdonnee()==arr.getOrdonnee()) {
    		return monPlateau [dep.getAbscisse()+1][dep.getOrdonnee()];
    	}else if (dep.getAbscisse()==arr.getAbscisse() && dep.getOrdonnee()==arr.getOrdonnee()+2) {
    		return monPlateau [dep.getAbscisse()][dep.getOrdonnee()-1];
    	}else if (dep.getAbscisse()==arr.getAbscisse() && dep.getOrdonnee()==arr.getOrdonnee()-2) {
    		return monPlateau [dep.getAbscisse()][dep.getOrdonnee()+1];
    	}
    	return null;
    }


   public void jouerCoupPrise(Case dep, Case arr) {
	   casePrise(dep, arr).setPion(null);
   }

    public void jouerCoup(Case dep, Case arr) {
    	arr.setPion(dep.getPion());
    	dep.setPion(null);
    }


    public void annulerCoup(Case dep, Case arr) {
    	// A completer
    }

    public void reinitialiser() {
    	// A completer
    }

}