import javax.swing.*;
import java.awt.*;

public class StockPion extends JPanel {
    private Case[][] monStockPion;
    private CouleurPion couleur;

    public StockPion(CouleurPion couleur) {
	this.couleur= couleur;
	this.setLayout(new GridLayout (6,2));
	monStockPion = new Case[6][2];
	for (int i=0; i<6; i++) {
		for (int j=0; j<2; j++) {
			monStockPion[i][j]= new Case(Color.black, i, j, 1);
			monStockPion[i][j].setPion(new Pion(couleur));
			this.add(monStockPion[i][j]);
			monStockPion[i][j].setBorder(Yote.empty);
		}
	}
    }
    
}